//#pragma once

#ifndef VEHICULE_HPP
#define VEHICULE_HPP

#include "glutils.hpp"
#include "mat4.hpp"
#include "vec3.hpp"

class vehicule {

private:
        struct transformation {
                mat4 rotation;
                vec3 rotation_center;
                vec3 translation;
        } transformation_vehicule;

        GLuint vbo_vehicule=0;
        GLuint vboi_vehicule=0;
        GLuint texture_id_vehicule=0;
        int nbr_triangle_vehicule;
        float x_init;
        void load_texture(const char* filename,GLuint *texture_id);
        void draw(void);
        void forward(float speed);

public:
        bool present;
        vehicule();
        void generateBus(void);
        void update(int level);
        vec3 getCoord();
        void removeVehicule(void);
};



#endif
