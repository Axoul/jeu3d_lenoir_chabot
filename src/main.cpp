/*****************************************************************************\
   CrossOver by Léo Lenoir & Axel Chabot
\*****************************************************************************/
#include <iostream>
#include <fstream>
#include <cstdlib>
#include <cstdio>
#include <cmath>
#include <chrono>
#include <ctime>
#define GLEW_STATIC 1
#include <GL/glew.h>
#include <GL/glut.h>

#include "glutils.hpp"

#include "mat4.hpp"
#include "vec3.hpp"
#include "vec2.hpp"
#include "triangle_index.hpp"
#include "vertex_opengl.hpp"
#include "image.hpp"
#include "mesh.hpp"

#include "vehicule.hpp"

GLuint shader_program_id;

//Varirables herbe
GLuint vbo_herbe=0;
GLuint vboi_herbe=0;
GLuint texture_id_herbe=0;
int nbr_triangle_herbe;

//Varirables routes
const int nb_route = 10;
GLuint vbo_route[nb_route] = { 0 };
GLuint vboi_route[nb_route] = { 0 };
GLuint texture_id_route[nb_route] = { 0 };
int nbr_triangle_route;

//Varirables panneau
GLuint vbo_sign = 0;
GLuint vboi_sign = 0;
GLuint texture_id_sign = 0;
int nbr_triangle_sign;

//Instances de vehicule
vehicule bus[10];

struct transformation
{
        mat4 rotation;
        vec3 rotation_center;
        vec3 translation;

        transformation() : rotation(),rotation_center(),translation(){
        }
};

struct text {
        GLuint vbo  = 0;       // Vertex buffer id
        GLuint vboi = 0;       // Index buffer id
        GLuint texture_id = 0; // Well, texture id...
        std::string value;     // Value of the text to display
        transformation transform; // Rotation & translation
        text() : transform(){
        }
};
text aff_sign;

// Translations et rotations
transformation transformation_herbe;
transformation transformation_route;
transformation transformation_sign;

transformation transformation_view;

mat4 projection;

float angle_view = 0.0f;

//Indice du dernier vehicule généré
int lastVehicule;

bool gameStart = false;
int level = 0;
std::time_t timeLevelUp = 10; //Temps d'incrémentation

std::time_t start, now, chrono = 0, before = 99, tps_final = 0;

//Variable pour souris
int ant_x = 0;

void load_texture(const char* filename,GLuint *texture_id);

void init_herbe();
void init_route();
void init_sign();
void initTraffic();
void init_text(text *t);

void draw_herbe();
void draw_route();
void draw_sign();
void updateTraffic();
void draw_texts(text *text_t);

void endGame();

static void init() {
        shader_program_id = read_shader("shader.vert", "shader.frag");
        projection = matrice_projection(60.0f*M_PI/180.0f,1.0f,0.01f,100.0f);
        glUniformMatrix4fv(get_uni_loc(shader_program_id,"projection"),1,false,pointeur(projection));
        transformation_view.translation = vec3(0.0f,0.0f,-22.0f);
        glEnable(GL_DEPTH_TEST);
        init_herbe();
        init_route();
        init_sign();
        aff_sign.transform.translation = vec3(-2.2f, 1.1f, 8.01f); //Positionnement des caractère sur le panneau
        aff_sign.value = " Press Space";
        init_text(&aff_sign);
}


static void display_callback() {
        glClearColor(0.5f, 0.6f, 0.9f, 1.0f); //Couleur bleue représentant le ciel
        glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
        //Application de la transformation de la vue
        glUniformMatrix4fv(get_uni_loc(shader_program_id,"rotation_view"),1,false,pointeur(transformation_view.rotation));
        vec3 cv = transformation_view.rotation_center;
        glUniform4f(get_uni_loc(shader_program_id,"rotation_center_view"), cv.x,cv.y,cv.z, 0.0f);
        vec3 tv = transformation_view.translation;
        glUniform4f(get_uni_loc(shader_program_id,"translation_view"), tv.x,tv.y,tv.z, 0.0f);
        draw_herbe();
        draw_route();
        draw_sign();

        if(gameStart) {
                updateTraffic();

                now = std::time(nullptr);
                chrono = now - start;
                if(chrono != before) {
                        char mbstr[10];
                        if(chrono%timeLevelUp==0) {
                                level++;
                        }
                        if (std::strftime(mbstr, sizeof(mbstr), "%M:%S", std::localtime(&chrono))) { //printf d'un time_t (s) en minutes et secondes préformaté
                                aff_sign.value = mbstr;
                                aff_sign.value += "  lvl ";
                                aff_sign.value += std::to_string(level);
                                aff_sign.transform.translation = vec3(-2.2f, 1.1f, 8.01f);
                                init_text(&aff_sign);
                        }
                }
                else {
                        draw_texts(&aff_sign);
                }
                before = chrono; //Mémoire

        }
        else {
                draw_texts(&aff_sign);
        }
        glutSwapBuffers();
}


static void keyboard_callback(unsigned char key, int, int) {
        switch (key) {
        case 'q':
        case 'Q':
        case 27:
                exit(0);
                break;

        case 32:
                if(!gameStart) { //Démarage du jeu
                        initTraffic(); //Génération de la premiere voiture
                        start = std::time(nullptr);
                        gameStart = true;
                }

        }
}


static void mouse_callback(int x, int) {
        float limit=1.8f;
        float ratio = 3.6f/700.0f;
        float move;
        int diff_x;
        diff_x = x - ant_x;
        move = (float)diff_x*ratio;
        if(gameStart) {
                if(move>=0) {
                        if(transformation_view.translation.x >= -limit) {
                                transformation_view.translation.x -= move;
                        }
                }
                else {
                        if(transformation_view.translation.x <= limit) {
                                transformation_view.translation.x -= move;
                        }
                }
        }
        ant_x = x;
}



static void timer_callback(int) {
        glutTimerFunc(25, timer_callback, 0);
        glutPostRedisplay();
}


int main(int argc, char** argv) {
        glewExperimental = GL_TRUE;
        glutInit(&argc, argv);
        glutInitDisplayMode(GLUT_DOUBLE | GLUT_RGB | GLUT_DEPTH);
        glutInitWindowSize(1000, 1000);
        glutCreateWindow("CrossOver");
        glutDisplayFunc(display_callback);
        glutKeyboardFunc(keyboard_callback);
        glutPassiveMotionFunc(mouse_callback);
        glutTimerFunc(25, timer_callback, 0);
        glewInit();
        init();
        glutMainLoop();
        return 0;
}

void draw_herbe() { //Application tranformation et des buffers de l'herbe
        glUniformMatrix4fv(get_uni_loc(shader_program_id,"rotation_model"),1,false,pointeur(transformation_herbe.rotation));
        vec3 c = transformation_herbe.rotation_center;
        glUniform4f(get_uni_loc(shader_program_id,"rotation_center_model"), c.x,c.y,c.z, 0.0f);
        vec3 t = transformation_herbe.translation;
        glUniform4f(get_uni_loc(shader_program_id,"translation_model"), t.x,t.y,t.z, 0.0f);
        glBindBuffer(GL_ARRAY_BUFFER,vbo_herbe);
        glEnableClientState(GL_VERTEX_ARRAY);
        glVertexPointer(3, GL_FLOAT, sizeof(vertex_opengl), 0);
        glEnableClientState(GL_NORMAL_ARRAY);
        glNormalPointer(GL_FLOAT, sizeof(vertex_opengl), buffer_offset(sizeof(vec3)));
        glEnableClientState(GL_COLOR_ARRAY);
        glColorPointer(3,GL_FLOAT, sizeof(vertex_opengl), buffer_offset(2*sizeof(vec3)));
        glEnableClientState(GL_TEXTURE_COORD_ARRAY);
        glTexCoordPointer(2,GL_FLOAT, sizeof(vertex_opengl), buffer_offset(3*sizeof(vec3)));
        glBindBuffer(GL_ELEMENT_ARRAY_BUFFER,vboi_herbe);
        glBindTexture(GL_TEXTURE_2D, texture_id_herbe);
        glDrawElements(GL_TRIANGLES, 3*nbr_triangle_herbe, GL_UNSIGNED_INT, 0);
}

void draw_route() { //Application tranformation et des buffers routes (10)
        glUniformMatrix4fv(get_uni_loc(shader_program_id,"rotation_model"),1,false,pointeur(transformation_route.rotation));
        vec3 c = transformation_route.rotation_center;
        glUniform4f(get_uni_loc(shader_program_id,"rotation_center_model"), c.x,c.y,c.z, 0.0f);
        vec3 t = transformation_route.translation;
        glUniform4f(get_uni_loc(shader_program_id,"translation_model"), t.x,t.y,t.z, 0.0f);

        for(int i=0; i<nb_route; i++) {
                glBindBuffer(GL_ARRAY_BUFFER,vbo_route[i]);
                glEnableClientState(GL_VERTEX_ARRAY);
                glVertexPointer(3, GL_FLOAT, sizeof(vertex_opengl), 0);
                glEnableClientState(GL_NORMAL_ARRAY);
                glNormalPointer(GL_FLOAT, sizeof(vertex_opengl), buffer_offset(sizeof(vec3)));
                glEnableClientState(GL_COLOR_ARRAY);
                glColorPointer(3,GL_FLOAT, sizeof(vertex_opengl), buffer_offset(2*sizeof(vec3)));
                glEnableClientState(GL_TEXTURE_COORD_ARRAY);
                glTexCoordPointer(2,GL_FLOAT, sizeof(vertex_opengl), buffer_offset(3*sizeof(vec3)));
                glBindBuffer(GL_ELEMENT_ARRAY_BUFFER,vboi_route[i]);
                glBindTexture(GL_TEXTURE_2D, texture_id_route[i]);
                glDrawElements(GL_TRIANGLES, 3*nbr_triangle_route, GL_UNSIGNED_INT, 0);
        }
}

void draw_sign() { //Application tranformation et des buffers du panneau
        glUniformMatrix4fv(get_uni_loc(shader_program_id,"rotation_model"),1,false,pointeur(transformation_sign.rotation));
        vec3 c = transformation_sign.rotation_center;
        glUniform4f(get_uni_loc(shader_program_id,"rotation_center_model"), c.x,c.y,c.z, 0.0f);
        vec3 t = transformation_sign.translation;
        glUniform4f(get_uni_loc(shader_program_id,"translation_model"), t.x,t.y,t.z, 0.0f);

        glBindBuffer(GL_ARRAY_BUFFER,vbo_sign);
        glEnableClientState(GL_VERTEX_ARRAY);
        glVertexPointer(3, GL_FLOAT, sizeof(vertex_opengl), 0);
        glEnableClientState(GL_NORMAL_ARRAY);
        glNormalPointer(GL_FLOAT, sizeof(vertex_opengl), buffer_offset(sizeof(vec3)));
        glEnableClientState(GL_COLOR_ARRAY);
        glColorPointer(3,GL_FLOAT, sizeof(vertex_opengl), buffer_offset(2*sizeof(vec3)));
        glEnableClientState(GL_TEXTURE_COORD_ARRAY);
        glTexCoordPointer(2,GL_FLOAT, sizeof(vertex_opengl), buffer_offset(3*sizeof(vec3)));
        glBindBuffer(GL_ELEMENT_ARRAY_BUFFER,vboi_sign);
        glBindTexture(GL_TEXTURE_2D, texture_id_sign);
        glDrawElements(GL_TRIANGLES, 3*nbr_triangle_sign, GL_UNSIGNED_INT, 0);
}

void init_herbe() {
        vec3 p0=vec3(-25.0f,-0.9f,-25.0f);
        vec3 p1=vec3( 25.0f,-0.9f,-25.0f);
        vec3 p2=vec3( 25.0f,-0.9f, 25.0f);
        vec3 p3=vec3(-25.0f,-0.9f, 25.0f);

        //normales pour chaque sommet
        vec3 n0=vec3(0.0f,1.0f,0.0f);
        vec3 n1=n0;
        vec3 n2=n0;
        vec3 n3=n0;

        //couleur pour chaque sommet
        vec3 c0=vec3(1.0f,1.0f,1.0f);
        vec3 c1=c0;
        vec3 c2=c0;
        vec3 c3=c0;

        //texture du sommet
        vec2 t0=vec2(0.0f,0.0f);
        vec2 t1=vec2(1.0f,0.0f);
        vec2 t2=vec2(1.0f,1.0f);
        vec2 t3=vec2(0.0f,1.0f);

        vertex_opengl v0=vertex_opengl(p0,n0,c0,t0);
        vertex_opengl v1=vertex_opengl(p1,n1,c1,t1);
        vertex_opengl v2=vertex_opengl(p2,n2,c2,t2);
        vertex_opengl v3=vertex_opengl(p3,n3,c3,t3);


        //tableau entrelacant coordonnees-normales
        vertex_opengl geometrie[]={v0,v1,v2,v3};


        //indice des triangles
        triangle_index tri0=triangle_index(0,1,2);
        triangle_index tri1=triangle_index(0,2,3);
        triangle_index index[]={tri0,tri1};
        nbr_triangle_herbe = 2;

        //attribution d'un buffer de donnees (1 indique la création d'un buffer)
        glGenBuffers(1,&vbo_herbe);                                             PRINT_OPENGL_ERROR();
        //affectation du buffer courant
        glBindBuffer(GL_ARRAY_BUFFER,vbo_herbe);                                PRINT_OPENGL_ERROR();
        //copie des donnees des sommets sur la carte graphique
        glBufferData(GL_ARRAY_BUFFER,sizeof(geometrie),geometrie,GL_STATIC_DRAW);  PRINT_OPENGL_ERROR();


        //attribution d'un autre buffer de donnees
        glGenBuffers(1,&vboi_herbe);                                            PRINT_OPENGL_ERROR();
        //affectation du buffer courant (buffer d'indice)
        glBindBuffer(GL_ELEMENT_ARRAY_BUFFER,vboi_herbe);                       PRINT_OPENGL_ERROR();
        //copie des indices sur la carte graphique
        glBufferData(GL_ELEMENT_ARRAY_BUFFER,sizeof(index),index,GL_STATIC_DRAW);  PRINT_OPENGL_ERROR();

        // Chargement de la texturetexturecarteChargementcarte
        load_texture("../data/grass.tga",&texture_id_herbe);
}

void init_route() {
        float haut = 0.25f; //Hauteur de la route
        float j = -25.0f; //Position de la première route
        for(int i=0; i<nb_route; i++, j+=5.0f) {
                vec3 p0 = vec3(-2.5f, -0.5f+haut, j);
                vec3 p1 = vec3(-2.012f, -0.5f+haut, j);
                vec3 p2 = vec3(-2.012f, -0.5f, j);
                vec3 p3 = vec3(2.012f, -0.5f, j);
                vec3 p4 = vec3(2.012f, -0.5f+haut, j);
                vec3 p5 = vec3(2.5f, -0.5f+haut, j);
                vec3 p6 = vec3(-2.5f, -0.5f+haut, j+5.0f);
                vec3 p7 = vec3(-2.012f, -0.5f+haut, j+5.0f);
                vec3 p8 = vec3(-2.012f, -0.5f, j+5.0f);
                vec3 p9 = vec3(2.012f, -0.5f, j+5.0f);
                vec3 p10 = vec3(2.012f, -0.5f+haut, j+5.0f);
                vec3 p11 = vec3(2.5f, -0.5f+haut, j+5.0f);

                //Normales de points
                vec3 n0 = vec3(0.0f, 1.0f, 0.0f);
                vec3 n11=n0, n22=n0, n31=n0, n42=n0, n5=n0, n6=n0, n71=n0, n82=n0, n91=n0, n102=n0, n110=n0;
                vec3 n12 = vec3(1.0f, 0.0f, 0.0f);
                vec3 n21=n12, n72=n12, n81=n12;
                vec3 n32 = vec3(-1.0f, 0.0f, 0.0f);
                vec3 n41=n32, n92=n32, n101=n32;

                vec3 c0 = vec3(1.0f, 1.0f, 1.0f);

                //Mappage texture
                vec2 t0 = vec2(0.0f, 0.0f);
                vec2 t1 = vec2(0.0f, 0.098f);
                vec2 t2 = vec2(0.0f, 0.123f);
                vec2 t3 = vec2(0.0f, 0.877f);
                vec2 t4 = vec2(0.0f, 0.902f);
                vec2 t5 = vec2(0.0f, 1.0f);
                vec2 t6 = vec2(1.0f, 0.0f);
                vec2 t7 = vec2(1.0f, 0.098f);
                vec2 t8 = vec2(1.0f, 0.123f);
                vec2 t9 = vec2(1.0f, 0.877f);
                vec2 t10 = vec2(1.0f, 0.902f);
                vec2 t11 = vec2(1.0f, 1.0f);

                //Génération vertex complets
                vertex_opengl v0 = vertex_opengl(p0, n0, c0, t0); //0 h
                vertex_opengl v11 = vertex_opengl(p1, n11, c0, t1); //1 h
                vertex_opengl v12 = vertex_opengl(p1, n12, c0, t1); //2 v
                vertex_opengl v21 = vertex_opengl(p2, n21, c0, t2); //3 v
                vertex_opengl v22 = vertex_opengl(p2, n22, c0, t2); //4 h
                vertex_opengl v31 = vertex_opengl(p3, n31, c0, t3); //5 h
                vertex_opengl v32 = vertex_opengl(p3, n32, c0, t3); //6 v
                vertex_opengl v41 = vertex_opengl(p4, n41, c0, t4); //7 v
                vertex_opengl v42 = vertex_opengl(p4, n42, c0, t4); //8 h
                vertex_opengl v5 = vertex_opengl(p5, n5, c0, t5); //9 h
                vertex_opengl v6 = vertex_opengl(p6, n6, c0, t6); //10 h
                vertex_opengl v71 = vertex_opengl(p7, n71, c0, t7); //11 h
                vertex_opengl v72 = vertex_opengl(p7, n72, c0, t7); //12 v
                vertex_opengl v81 = vertex_opengl(p8, n81, c0, t8); //13 v
                vertex_opengl v82 = vertex_opengl(p8, n82, c0, t8); //14 h
                vertex_opengl v91 = vertex_opengl(p9, n91, c0, t9); //15 h
                vertex_opengl v92 = vertex_opengl(p9, n92, c0, t9); //16 v
                vertex_opengl v101 = vertex_opengl(p10, n101, c0, t10); //17 v
                vertex_opengl v102 = vertex_opengl(p10, n102, c0, t10); //18 h
                vertex_opengl v110 = vertex_opengl(p11, n110, c0, t11); //19 h

                vertex_opengl geometrie[]={v0,v11,v12,v21,v22,v31,v32,v41,v42,v5,v6,v71,v72,v81,v82,v91,v92,v101,v102,v110};

                //Création triangle
                triangle_index tri0=triangle_index(0,1,11);
                triangle_index tri1=triangle_index(0,11,10);
                triangle_index tri2=triangle_index(2,13,3);
                triangle_index tri3=triangle_index(2,13,12);
                triangle_index tri4=triangle_index(4,15,14);
                triangle_index tri5=triangle_index(4,15,5);
                triangle_index tri6=triangle_index(6,17,16);
                triangle_index tri7=triangle_index(6,17,7);
                triangle_index tri8=triangle_index(8,19,18);
                triangle_index tri9=triangle_index(8,19,9);
                triangle_index index[]={tri0,tri1,tri2,tri3,tri4,tri5,tri6,tri7,tri8,tri9};
                nbr_triangle_route = 10;

                glGenBuffers(1,&vbo_route[i]);
                glBindBuffer(GL_ARRAY_BUFFER,vbo_route[i]);
                glBufferData(GL_ARRAY_BUFFER,sizeof(geometrie),geometrie,GL_STATIC_DRAW);
                glGenBuffers(1,&vboi_route[i]);
                glBindBuffer(GL_ELEMENT_ARRAY_BUFFER,vboi_route[i]);
                glBufferData(GL_ELEMENT_ARRAY_BUFFER,sizeof(index),index,GL_STATIC_DRAW);
                //Attribution des textures
                if(i==9)
                        load_texture("../data/passage.tga",&texture_id_route[i]);
                else if(i%2==0)
                        load_texture("../data/route3.tga",&texture_id_route[i]);
                else
                        load_texture("../data/route2.tga",&texture_id_route[i]);
        }

}

void init_sign() {
        //Points du panneau
        float depth = 8.0f;
        float ext_x = 2.378f;
        float int_x = 2.134f;
        float up_y = 1.9f;
        float int_y = 1.0f;
        float dw_y = -0.25f;
        vec3 p0 = vec3(-ext_x, dw_y, depth);
        vec3 p1 = vec3(-int_x, dw_y, depth);
        vec3 p2 = vec3(-ext_x, int_y, depth);
        vec3 p3 = vec3(-int_x, int_y, depth);
        vec3 p4 = vec3(-ext_x, up_y, depth);
        vec3 p5 = vec3(ext_x, up_y, depth);
        vec3 p6 = vec3(int_x, int_y, depth);
        vec3 p7 = vec3(ext_x, int_y, depth);
        vec3 p8 = vec3(int_x, dw_y, depth);
        vec3 p9 = vec3(ext_x, dw_y, depth);

        vec3 n = vec3(0.0f, 0.0f, 1.0f);
        vec3 c = vec3(1.0f, 1.0f, 1.0f);

        vec2 t0 = vec2(0.0f, 0.0f);
        vec2 t1 = vec2(0.121f, 0.0f);
        vec2 t2 = vec2(0.0f, 1.0f);

        vec2 t3 = vec2(0.121f, 1.0f);
        vec2 t4 = vec2(0.121f, 0.3f);
        vec2 t5 = vec2(1.0f, 1.0f);
        vec2 t6 = vec2(1.0f, 0.3f);

        vertex_opengl v0 = vertex_opengl(p0, n, c, t2 ); //0
        vertex_opengl v1 = vertex_opengl(p1, n, c, t3); //1
        vertex_opengl v21 = vertex_opengl(p2, n, c, t0); //2
        vertex_opengl v3 = vertex_opengl(p3, n, c, t1); //3

        vertex_opengl v22 = vertex_opengl(p2, n, c, t4); //4
        vertex_opengl v4 = vertex_opengl(p4, n, c, t1); //5
        vertex_opengl v5 = vertex_opengl(p5, n, c, t5); //6
        vertex_opengl v6 = vertex_opengl(p6, n, c, t0); //7
        vertex_opengl v71 = vertex_opengl(p7, n, c, t6); //8

        vertex_opengl v72 = vertex_opengl(p7, n, c, t1); //9
        vertex_opengl v8 = vertex_opengl(p8, n, c, t2); //10
        vertex_opengl v9 = vertex_opengl(p9, n, c, t3); //11
        vertex_opengl geometrie[] = {v0,v1,v21,v3,v22,v4,v5,v6,v71,v72,v8,v9};


        triangle_index tri0 = triangle_index(0,2,3);
        triangle_index tri1 = triangle_index(0,1,3);

        triangle_index tri2 = triangle_index(4,5,6);
        triangle_index tri3 = triangle_index(4,6,8);

        triangle_index tri4 = triangle_index(7,9,11);
        triangle_index tri5 = triangle_index(7,10,11);
        triangle_index index[] = {tri0,tri1,tri2,tri3,tri4,tri5};
        nbr_triangle_sign = 6;

        glGenBuffers(1,&vbo_sign);
        glBindBuffer(GL_ARRAY_BUFFER,vbo_sign);
        glBufferData(GL_ARRAY_BUFFER,sizeof(geometrie),geometrie,GL_STATIC_DRAW);
        glGenBuffers(1,&vboi_sign);
        glBindBuffer(GL_ELEMENT_ARRAY_BUFFER,vboi_sign);
        glBufferData(GL_ELEMENT_ARRAY_BUFFER,sizeof(index),index,GL_STATIC_DRAW);
        load_texture("../data/sign.tga",&texture_id_sign);
}

void initTraffic() { //Génération du premier bus
        lastVehicule = 0;
        bus[lastVehicule].generateBus();
}

void updateTraffic() {
        //Test de collision
        bool full = true;
        for(int i=0; i<10; i++) {
                if(bus[i].present) {
                        bus[i].update(level);

                        float limitz = -transformation_view.translation.z;
                        float deltaz = 0.5f;
                        float limitx = -transformation_view.translation.x;
                        float deltax = 0.25f;
                        if((bus[i].getCoord().z >= limitz-deltaz) && (bus[i].getCoord().z <= limitz+deltaz)) {
                                if((bus[i].getCoord().x < (limitx + deltax)) && (bus[i].getCoord().x > (limitx - deltax))) {
                                        endGame();
                                }
                        }
                }
                else {
                        full = false;
                }
        }
        //Génération automatique de bus
        if(!full && bus[lastVehicule].getCoord().z > -15.0f) {
                int j = 0;
                while(bus[j].present) {
                        j++;
                }
                bus[j].generateBus();
                lastVehicule = j;
        }
}


void load_texture(const char* filename,GLuint *texture_id) {
        Image  *image = image_load_tga(filename);
        if (image) {
                glPixelStorei(GL_UNPACK_ALIGNMENT, 1);
                glGenTextures(1, texture_id);
                glBindTexture(GL_TEXTURE_2D, *texture_id);
                glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
                glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);
                glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
                glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);

                if(image->type==IMAGE_TYPE_RGB) {
                        glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB, image->width, image->height, 0, GL_RGB, GL_UNSIGNED_BYTE, image->data);
                }
                else if(image->type==IMAGE_TYPE_RGBA) {
                        glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, image->width, image->height, 0, GL_RGBA, GL_UNSIGNED_BYTE, image->data);
                }
                else {
                        std::cout<<"Image type not handled"<<std::endl;
                }

                delete image;
        }
        else {
                std::cerr<<"Erreur chargement de l'image, etes-vous dans le bon repertoire?"<<std::endl;
                abort();
        }

        glUniform1i (get_uni_loc(shader_program_id, "texture"), 0);
}

void init_text(text *t) {
        float font_height = 0.65f;
        float font_width  = 0.35f;
        int length = t->value.size();   // Number of char to draw
        vertex_opengl geometrie[4*length]; // We need 4 vertices per char
        triangle_index index[2*length]; // We need 2 triangles per char

        int ascii_offset = 32;          // ASCII code of 1st char in texture file is 32
        int width        = 30;          // 30 char per line in texture file
        float x_tick     = 0.0333f;     // .. so 1/30 char horizontally
        float y_tick     = 0.2f;        // And 1/5 char vertically

        // Here we go, for each char, we create a rectangle.
        for(int i = 0; i < length; i++) {
                //Vertices coordinates
                vec3 p0=vec3( i*font_width,        0.0f, 0.0f);
                vec3 p1=vec3( i*font_width, font_height, 0.0f);
                vec3 p2=vec3( (i+1)*font_width, font_height, 0.0f);
                vec3 p3=vec3( (i+1)*font_width,        0.0f, 0.0f);

                //Vertices normal
                vec3 n0=vec3(0.0f,0.0f,1.0f);
                vec3 n1=n0;
                vec3 n2=n0;
                vec3 n3=n0;

                //Vertices color
                vec3 c0=vec3(1.0f,1.0f,1.0f);
                vec3 c1=c0;
                vec3 c2=c0;
                vec3 c3=c0;

                //Vertices texture
                int ascii_code      = (int)t->value[i];        // Current char ASCII value
                int texture_code    = ascii_code - ascii_offset;// Current char index in our texture
                float texture_x = (texture_code % width) * x_tick; // Current char horizontal position
                float texture_y = (int)(texture_code / width) * y_tick; // Current char vertical position
                vec2 t0=vec2(texture_x, texture_y + y_tick);
                vec2 t1=vec2(texture_x, texture_y         );
                vec2 t2=vec2(texture_x + x_tick, texture_y         );
                vec2 t3=vec2(texture_x + x_tick, texture_y + y_tick);

                geometrie[i*4]   = vertex_opengl(p0, n0, c0, t0);
                geometrie[i*4+1] = vertex_opengl(p1, n1, c1, t1);
                geometrie[i*4+2] = vertex_opengl(p2, n2, c2, t2);
                geometrie[i*4+3] = vertex_opengl(p3, n3, c3, t3);

                index[i*2]   = triangle_index(i*4, i*4+1, i*4+2);
                index[i*2+1] = triangle_index(i*4, i*4+2, i*4+3);
        }
        glGenBuffers(1,&t->vbo);
        glBindBuffer(GL_ARRAY_BUFFER,t->vbo);
        glBufferData(GL_ARRAY_BUFFER,sizeof(geometrie),geometrie,GL_STATIC_DRAW);
        glGenBuffers(1,&t->vboi);
        glBindBuffer(GL_ELEMENT_ARRAY_BUFFER,t->vboi);
        glBufferData(GL_ELEMENT_ARRAY_BUFFER,sizeof(index),index,GL_STATIC_DRAW);
        load_texture("../data/fontA.tga",&t->texture_id);
}

void draw_texts(text *text_t) {
        //Send uniforma parameters
        glUniformMatrix4fv(get_uni_loc(shader_program_id,"rotation_model"),1,false,pointeur(text_t->transform.rotation));
        vec3 c = text_t->transform.rotation_center;
        glUniform4f(get_uni_loc(shader_program_id,"rotation_center_model"), c.x,c.y,c.z, 0.0f);
        vec3 t = text_t->transform.translation;
        glUniform4f(get_uni_loc(shader_program_id,"translation_model"), t.x,t.y,t.z, 0.0f);
        glBindBuffer(GL_ARRAY_BUFFER,text_t->vbo);
        glEnableClientState(GL_VERTEX_ARRAY);
        glVertexPointer(3, GL_FLOAT, sizeof(vertex_opengl), 0);
        glEnableClientState(GL_NORMAL_ARRAY);
        glNormalPointer(GL_FLOAT, sizeof(vertex_opengl), buffer_offset(sizeof(vec3)));
        glEnableClientState(GL_COLOR_ARRAY);
        glColorPointer(3,GL_FLOAT, sizeof(vertex_opengl), buffer_offset(2*sizeof(vec3)));
        glEnableClientState(GL_TEXTURE_COORD_ARRAY);
        glTexCoordPointer(2,GL_FLOAT, sizeof(vertex_opengl), buffer_offset(3*sizeof(vec3)));
        glBindBuffer(GL_ELEMENT_ARRAY_BUFFER,text_t->vboi);
        glBindTexture(GL_TEXTURE_2D, text_t->texture_id);
        int nbr_triangles = text_t->value.size()*2;         // We draw two triangles per char.
        glDrawElements(GL_TRIANGLES, 3*nbr_triangles, GL_UNSIGNED_INT, 0);
}

void endGame() { //Gestion de la fin du jeu
        gameStart = false;
        tps_final = chrono; //Sauvegarde du temps final
        level = 0;
        aff_sign.transform.translation = vec3(-2.2f, 1.1f, 8.01f);
        char str[10];

        //Suppresion de tout les vehicules
        for(int i=0; i<10; i++) {
                if(bus[i].present) {
                        bus[i].removeVehicule();
                }
        }

        //Gestion du fichier de meilleur score
        std::ofstream highscore_out;
        std::ifstream highscore_in;
        std::string buff;
        int bestTps;
        std::string filename("highscore");
        highscore_in.open(filename.c_str());
        if(highscore_in) {
                highscore_in >> buff;
                bestTps = std::atoi(buff.c_str());
                highscore_in.close();
                if((int)tps_final>bestTps) { //Si l'utilisateur à gagné
                        highscore_out.open(filename.c_str());
                        highscore_out << tps_final;
                        highscore_out.close();
                        aff_sign.value = "HIScore ";
                }
                else {
                        aff_sign.value = "Score ";
                }
        }

        if (std::strftime(str, sizeof(str), "%M:%S", std::localtime(&tps_final))) {
                aff_sign.value += str;
        }
        init_text(&aff_sign);
}
