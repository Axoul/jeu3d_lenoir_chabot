#include <iostream>
#include <cstdlib>
#include <cstdio>
#include <random>
#include <chrono>
#define GLEW_STATIC 1
#include <GL/glew.h>
#include <GL/glut.h>
#include "glutils.hpp"
#include "mat4.hpp"
#include "vec3.hpp"
#include "vec2.hpp"
#include "triangle_index.hpp"
#include "vertex_opengl.hpp"
#include "image.hpp"
#include "mesh.hpp"
#include "vehicule.hpp"

const float range = 1.5f;
std::default_random_engine re(std::chrono::system_clock::now().time_since_epoch().count()); //Initilisation du seed
std::uniform_real_distribution<float> distrib{-range,range}; //Génération d'aléatoire borné de type float

extern GLuint shader_program_id; //Récupération de notre shader du main

vehicule::vehicule() { //Constructeur
        transformation_vehicule.translation = vec3();
        transformation_vehicule.rotation_center = vec3();
        transformation_vehicule.rotation = mat4();
        present = false;
}

void vehicule::generateBus(void) { //Génération random de bus
        mesh m = load_obj_file("../data/bus.obj");
        float x_init = distrib(re); //Random
        transformation_vehicule.translation = vec3(x_init,-0.5f,-20.0f); //Placement au bout de la route
        transformation_vehicule.rotation = matrice_rotation(-0.6f, 0.0f,1.0f,0.0f); //Corrections du modèle obj
        update_normals(&m);
        fill_color(&m,vec3(1.0f,1.0f,1.0f));
        glGenBuffers(1,&vbo_vehicule);
        glBindBuffer(GL_ARRAY_BUFFER,vbo_vehicule);
        glBufferData(GL_ARRAY_BUFFER,m.vertex.size()*sizeof(vertex_opengl),&m.vertex[0],GL_STATIC_DRAW);
        glGenBuffers(1,&vboi_vehicule);
        glBindBuffer(GL_ELEMENT_ARRAY_BUFFER,vboi_vehicule);
        glBufferData(GL_ELEMENT_ARRAY_BUFFER,m.connectivity.size()*sizeof(triangle_index),&m.connectivity[0],GL_STATIC_DRAW);
        nbr_triangle_vehicule = m.connectivity.size();
        load_texture("../data/bus.tga",&texture_id_vehicule);
        present = true;
}

void vehicule::draw(void) { //Dessin de la voiture et passage transformation
        glUniformMatrix4fv(get_uni_loc(shader_program_id,"rotation_model"),1,false,pointeur(transformation_vehicule.rotation));
        vec3 c = transformation_vehicule.rotation_center;
        glUniform4f(get_uni_loc(shader_program_id,"rotation_center_model"), c.x,c.y,c.z, 0.0f);
        vec3 t = transformation_vehicule.translation;
        glUniform4f(get_uni_loc(shader_program_id,"translation_model"), t.x,t.y,t.z, 0.0f);
        glBindBuffer(GL_ARRAY_BUFFER,vbo_vehicule);
        glEnableClientState(GL_VERTEX_ARRAY);
        glVertexPointer(3, GL_FLOAT, sizeof(vertex_opengl), 0);
        glEnableClientState(GL_NORMAL_ARRAY); PRINT_OPENGL_ERROR();
        glNormalPointer(GL_FLOAT, sizeof(vertex_opengl), buffer_offset(sizeof(vec3)));
        glEnableClientState(GL_COLOR_ARRAY); PRINT_OPENGL_ERROR();
        glColorPointer(3,GL_FLOAT, sizeof(vertex_opengl), buffer_offset(2*sizeof(vec3)));
        glEnableClientState(GL_TEXTURE_COORD_ARRAY);
        glTexCoordPointer(2,GL_FLOAT, sizeof(vertex_opengl), buffer_offset(3*sizeof(vec3)));
        glBindBuffer(GL_ELEMENT_ARRAY_BUFFER,vboi_vehicule);
        glBindTexture(GL_TEXTURE_2D, texture_id_vehicule);
        glDrawElements(GL_TRIANGLES, 3*nbr_triangle_vehicule, GL_UNSIGNED_INT, 0);
}

void vehicule::update(int level) {
        if(abs(transformation_vehicule.translation.z)>25) { //Si vehicule hors de la map
                removeVehicule();
        }
        else {
                forward(exp(((float)level)*0.1f)*0.1f);
                draw();
        }
}

void vehicule::forward(float speed) { //Avancer
        transformation_vehicule.translation.z += speed;
}

vec3 vehicule::getCoord() {
        return transformation_vehicule.translation;
}

void vehicule::removeVehicule() { //Getter
        glDeleteBuffers(1,&vbo_vehicule);
        present = false;
}


void vehicule::load_texture(const char* filename,GLuint *texture_id) {
        Image  *image = image_load_tga(filename);
        if (image) {
                glPixelStorei(GL_UNPACK_ALIGNMENT, 1); PRINT_OPENGL_ERROR();
                glGenTextures(1, texture_id); PRINT_OPENGL_ERROR();
                glBindTexture(GL_TEXTURE_2D, *texture_id); PRINT_OPENGL_ERROR();
                glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT); PRINT_OPENGL_ERROR();
                glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT); PRINT_OPENGL_ERROR();
                glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR); PRINT_OPENGL_ERROR();
                glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR); PRINT_OPENGL_ERROR();

                if(image->type==IMAGE_TYPE_RGB) {
                        glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB, image->width, image->height, 0, GL_RGB, GL_UNSIGNED_BYTE, image->data); PRINT_OPENGL_ERROR();
                }
                else if(image->type==IMAGE_TYPE_RGBA) {
                        glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, image->width, image->height, 0, GL_RGBA, GL_UNSIGNED_BYTE, image->data); PRINT_OPENGL_ERROR();
                }
                else {
                        std::cout<<"Image type not handled"<<std::endl;
                }

                delete image;
        }
        else {
                std::cerr<<"Erreur chargement de l'image, etes-vous dans le bon repertoire?"<<std::endl;
                abort();
        }

        glUniform1i (get_uni_loc(shader_program_id, "texture"), 0); PRINT_OPENGL_ERROR();
}
